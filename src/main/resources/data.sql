/* LOTR franchise */
INSERT INTO franchise (name, description) VALUES ('The lord of the rings', 'One of the first and all time greatest fantasy universes - a must see classic');

/* LOTR franchise movies*/
INSERT INTO movie (title, genre, release_year, picture, director, trailer) VALUES ('lord of the rings: fellowship of the ring', 'fantasy, action', 2001, 'img/url/fel', 'peter jackson', 'video/trailer/link');
INSERT INTO movie (title, genre, release_year, picture, director, trailer) VALUES ('lord of the rings: the two towers', 'fantasy, action', 2002, 'img/url/tt', 'peter jackson', 'video/trailer/link');
INSERT INTO movie (title, genre, release_year, picture,director, trailer) VALUES ('lord of the rings: the return of the king', 'fantasy, action', 2003, 'img/url/rotk', 'peter jackson', 'video/trailer/link');
INSERT INTO movie (title, genre, release_year, picture,director, trailer) VALUES ('the hobbit: an unexpected journey', 'fantasy, action', 2012, 'img/url/itsalldownhillfromhere', 'peter jackson', 'video/trailer/link');

/* LOTR franchise characters*/
INSERT INTO character (full_name, alias, gender, image) VALUES ('aragorn', 'strider', 'male', 'img/url/ara');
INSERT INTO character (full_name, gender, image) VALUES ('galadriel', 'female', 'img/url/gala');
INSERT INTO character (full_name, gender, image) VALUES ('frodo', 'male', 'img/url/frodo');
INSERT INTO character (full_name, alias, gender, image) VALUES ('thorin', 'the king under the mountain', 'male', 'img/url/thorin');

/* LOTR movie<->character many-to-many mappings*/
insert into movie_character(movie_id, character_id) values (1, 1);
insert into movie_character(movie_id, character_id) values (2, 1);
insert into movie_character(movie_id, character_id) values (3, 1);
insert into movie_character(movie_id, character_id) values (1, 2);
insert into movie_character(movie_id, character_id) values (1, 3);
insert into movie_character(movie_id, character_id) values (2, 3);
insert into movie_character(movie_id, character_id) values (3, 3);
insert into movie_character(movie_id, character_id) values (4, 4);
insert into movie_character(movie_id, character_id) values (4, 3);

/* LOTR franchise<->movie one-to-many mappings*/
insert into franchise_movie(franchise_id, movie_id) values (1,1);
insert into franchise_movie(franchise_id, movie_id) values (1,2);
insert into franchise_movie(franchise_id, movie_id) values (1,3);
insert into franchise_movie(franchise_id, movie_id) values (1,4);

/* Harry Potter franchise*/
INSERT INTO franchise (name, description) VALUES ('harry potter', 'Excellent series of movies about magic and the dangers of living in England');

/* HP franchise movies */
INSERT INTO movie (title, genre, release_year, picture, director, trailer) VALUES ('harry potter and the philosopher´s stone', 'fantasy, action', 2001, 'img/url/phil', 'chris columbus', 'video/trailer/link');
INSERT INTO movie (title, genre, release_year, picture,director, trailer) VALUES ('harry potter and the order of the phoenix', 'fantasy, action', 2007, 'img/url/phoen', 'mike newell', 'video/trailer/link');
INSERT INTO movie (title, genre, release_year, picture, director, trailer) VALUES ('harry potter and the deathly hallows 1', 'fantasy, action', 2011, 'img/url/deha', 'david yates', 'video/trailer/link');


/* HP franchise<->movie one-to-many mappings*/
insert into franchise_movie(franchise_id, movie_id) values (2,5);
insert into franchise_movie(franchise_id, movie_id) values (2,6);
insert into franchise_movie(franchise_id, movie_id) values (2,7);

/* HP characters */
INSERT INTO character (full_name, gender, image) VALUES ('hermione', 'female', 'img/url/hermi');
INSERT INTO character (full_name, gender, image) VALUES ('sirius black', 'male', 'img/url/siri');

/* LOTR movie<->character many-to-many mappings*/
insert into movie_character(movie_id, character_id) values (5, 5);
insert into movie_character(movie_id, character_id) values (6, 5);
insert into movie_character(movie_id, character_id) values (7, 5);
insert into movie_character(movie_id, character_id) values (6, 6);

