package no.noroff.assignment7movies.controller;

import no.noroff.assignment7movies.models.Character;
import no.noroff.assignment7movies.models.Movie;
import no.noroff.assignment7movies.repository.CharacterRepository;
import no.noroff.assignment7movies.repository.MovieRepository;
import org.apache.tomcat.util.http.parser.HttpParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/character")
public class CharacterController {

    //inject character repo
    @Autowired
    private CharacterRepository characterRepository;

    //get all characters
    @GetMapping("/all")
    public ResponseEntity<List<Character>> getCharacters() {
        HttpStatus status = HttpStatus.OK;
        List<Character> returnList = characterRepository.findAll();
        if (returnList.size() == 0) {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnList, status);
    }

    //add a new character
    @PostMapping("/add")
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        Character returnCharacter;
        HttpStatus status = HttpStatus.OK;

        //post character and check if it was posted successfully
        returnCharacter = characterRepository.save(character);
        if (!characterRepository.existsById(character.id)) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<>(returnCharacter, status);
    }

    //get a character by its id
    @GetMapping("/id/{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable Integer id) {
        Character returnCharacter = new Character();
        HttpStatus status;

        if (characterRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnCharacter = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacter, status);
    }

    //delete a character and its references in join tables
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable Integer id) {

        HttpStatus status;
        String message;
        characterRepository.deleteById(id);
        if (characterRepository.existsById(id)){
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            message = "Cannot find character with id " + id;
        } else {
            status = HttpStatus.OK;
            message = "Deleted the character with id " + id;
        }
        return new ResponseEntity<>(message, status);
    }


    //update a character
    @PutMapping("/update/{id}")
    public ResponseEntity<Character> updateCharacter(@RequestBody Character inputCharacter,
                                                     @PathVariable Integer id) {
        Character updatedCharacter = null;
        HttpStatus status;

        //check if character ID is valid in db
        if (!characterRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
        } else {
            updatedCharacter = characterRepository.findById(id).get();
            status = HttpStatus.OK;

            //clumsy update - check field by field if this field needs to be updated
            if (inputCharacter.fullName != null) {
                updatedCharacter.fullName = inputCharacter.fullName;
            }
            if (inputCharacter.alias != null) {
                updatedCharacter.alias = inputCharacter.alias;
            }
            if (inputCharacter.gender != null) {
                updatedCharacter.gender = inputCharacter.gender;
            }
            if (inputCharacter.image != null) {
                updatedCharacter.image = inputCharacter.image;
            }
            characterRepository.save(updatedCharacter);
        }
        return new ResponseEntity<>(updatedCharacter, status);
    }
}

