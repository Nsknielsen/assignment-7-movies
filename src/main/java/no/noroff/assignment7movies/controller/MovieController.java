package no.noroff.assignment7movies.controller;

import no.noroff.assignment7movies.models.Character;
import no.noroff.assignment7movies.models.Movie;
import no.noroff.assignment7movies.repository.CharacterRepository;
import no.noroff.assignment7movies.repository.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*")
@RequestMapping("/movie")
@RestController
public class MovieController {

    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieController(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @GetMapping("/")
    public List<Movie> getAllMovies(){
        List<Movie> movies = movieRepository.findAll();
        return movies;
    }


    @PostMapping("/add")
    public Movie addMovie(@RequestBody Movie movie)
    {
        movie = movieRepository.save(movie);
        return movie;
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable Integer id){
                Movie movie = null;
                HttpStatus status = HttpStatus.NOT_FOUND;

        if(movieRepository.existsById(id)){
            movie = movieRepository.findById(id).get();
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(movie, status);
    }

    //delete a movie and its references in join tables
    @DeleteMapping("/delete/{id}")
    public void deleteMovie(@PathVariable Integer id){
        movieRepository.deleteById(id);
    }

    //update a movie
    @PutMapping("/update/{id}")
    public ResponseEntity<Movie> updateMovie(@RequestBody Movie inputMovie,
                                                     @PathVariable Integer id){
        Movie updatedMovie = null;
        HttpStatus status;

        //check if movie ID is valid in db
        if(!movieRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
        } else {
            updatedMovie =movieRepository.findById(id).get();
            status = HttpStatus.OK;

            //clumsy update - check field by field if this field needs to be updated
            if(inputMovie.title != null){
                updatedMovie.title = inputMovie.title;
            }
            if(inputMovie.genre != null){
                updatedMovie.genre = inputMovie.genre;
            }
            if(inputMovie.releaseYear != null){
                updatedMovie.releaseYear = inputMovie.releaseYear;
            }
            if(inputMovie.director != null){
                updatedMovie.director = inputMovie.director;
            }
            if(inputMovie.picture != null){
                updatedMovie.picture = inputMovie.picture;
            }
            if(inputMovie.trailer != null){
                updatedMovie.trailer = inputMovie.trailer;
            }
            movieRepository.save(updatedMovie);
        }
        return new ResponseEntity<>(updatedMovie, status);
    }

    //update the characters in a movie
    @PutMapping("/{movieId}/characters")
    public ResponseEntity<Movie> updateCharactersInMovie(
            @RequestBody int[] newCharacterIds,
            @PathVariable int movieId){
        Set<Character> newCharacters = new HashSet<>();
        HttpStatus status = HttpStatus.OK;
        Movie movie = new Movie();

        //check if movie exists and get it, then change movie characters to the ones we got above
        if (movieRepository.existsById(movieId)) {
            movie = movieRepository.findById(movieId).get();
            //get all characters mentioned in the input set, if they exist in the db
            for (int characterId : newCharacterIds){
                if (characterRepository.existsById(characterId)){
                    Character character = characterRepository.findById(characterId).get();
                    newCharacters.add(character);
                }
            }
            movie.characters = newCharacters;
            movieRepository.save(movie);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movie, status);
    }

    //get the characters in a movie
    @GetMapping("/{movieId}/characters")
    public ResponseEntity<HashSet<Character>> getCharactersInMovie(
            @PathVariable int movieId){
        HashSet<Character> returnCharacters = new HashSet<>();
        HttpStatus status = HttpStatus.OK;
        Movie movie;

        //if movie exists, loop through its characters and add them to returnCharacters set
        if (movieRepository.existsById(movieId)) {
            movie = movieRepository.findById(movieId).get();
            for (Character character: movie.characters){
                returnCharacters.add(character);
            }
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnCharacters, status);
    }

}