package no.noroff.assignment7movies.controller;

import no.noroff.assignment7movies.models.Character;
import no.noroff.assignment7movies.models.Franchise;
import no.noroff.assignment7movies.models.Movie;
import no.noroff.assignment7movies.repository.FranchiseRepository;
import no.noroff.assignment7movies.repository.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin(origins = "*") // Hvad var det nu det gjorde? - husker noget med, at Greg sagde, at man ikke bare ville
// tillade noget at være helt åbent (med "*" altså)
@RequestMapping("/franchise")
public class FranchiseController {

    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseController(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    //get all franchises - not yet using proper http status codes
    @GetMapping("/")
    public ResponseEntity<List<Franchise>> getFranchises() {

        HttpStatus status;

        status = HttpStatus.OK;
        List<Franchise> returnFranchise = franchiseRepository.findAll();

        return new ResponseEntity<>(returnFranchise, status);
    }

    //add a new franchise
    @PostMapping("/add")
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status = HttpStatus.OK;

        //if missing obligatory name field
        if (franchise.name == null){
            status = HttpStatus.BAD_REQUEST;

            //else post character and check if it was posted successfully
        } else {
            returnFranchise = franchiseRepository.save(franchise);
            if (!franchiseRepository.existsById(franchise.id)){
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }
        }

        return new ResponseEntity<>(returnFranchise, status);
    }

    //get a franchise by its id
    @GetMapping("/id/{id}")
    public ResponseEntity<Franchise> getFranchiseById(@PathVariable Integer id) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    //get a franchise by name
    @GetMapping("/name/{name}")
    public ResponseEntity<Franchise> findFranchiseByName(@PathVariable String name) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsByName(name)) {
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findByName(name);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    //delete a franchise and its references in join tables
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteFranchise(@PathVariable Integer id){
        HttpStatus status;
        String message;

        //if franchise exists, make sure it doesn't and tell us that the operation was successful
        if(franchiseRepository.existsById(id)){
            franchiseRepository.deleteById(id);
            status = HttpStatus.OK;
            message = "Deleted the franchise with id" + id;
        } else {
            status = HttpStatus.NOT_FOUND;
            message = "Cannot find franchise with id " + id;
        }
        return new ResponseEntity<>(message, status);
    }


    //update a franchise
    @PutMapping("/update/{id}")
    public ResponseEntity<Franchise> updateFranchise(@RequestBody Franchise inputFranchise,
                                                     @PathVariable Integer id){
        Franchise updatedFranchise = null;
        HttpStatus status;

        //check if character ID is valid in db
        if(!franchiseRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
        } else {
            updatedFranchise =franchiseRepository.findById(id).get();
            status = HttpStatus.OK;

            //clumsy update - check field by field if this field needs to be updated
            if(inputFranchise.name != null){
                updatedFranchise.name = inputFranchise.name;
            }
            if(inputFranchise.description != null){
                updatedFranchise.description = inputFranchise.description;
            }
            franchiseRepository.save(updatedFranchise);
        }
        return new ResponseEntity<>(updatedFranchise, status);
    }

    //get the movies in a franchise
    @GetMapping("/{id}/movies")
    public ResponseEntity<HashSet<Movie>> getMoviesInFranchise(
            @PathVariable int id){
        HashSet<Movie> returnMovies = new HashSet<>();
        HttpStatus status = HttpStatus.OK;
        Franchise franchise;

        // if franchise exists, loop through movies and add them to the response
        if(franchiseRepository.existsById(id)){
            franchise = franchiseRepository.findById(id).get();
            for(Movie movie : franchise.movies){
                returnMovies.add(movie);
            }
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovies, status);
    }

    // Update the movies in at franchise
    @PutMapping("/{franchiseId}/movies")
    public ResponseEntity<Franchise> updateMoviesInFranchise(
            @RequestBody int[] movieIds,
            @PathVariable int franchiseId){
        HttpStatus status = HttpStatus.OK;
        Franchise franchise = new Franchise();
        Set<Movie> returnMovies = new HashSet<>();

        // Check if franchise exists
        if(franchiseRepository.existsById(franchiseId)){
            franchise = franchiseRepository.findById(franchiseId).get();
            // add movies with id's provided in request body to returnMovies set
            for(int movieId : movieIds){
                Movie movie = movieRepository.findById(movieId).get();
                returnMovies.add(movie);
            }
        // use returnMovies set as new movies attribute in franchise
            franchise.movies = returnMovies;

        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(franchise, status);
    }

    //get all characters in franchise
    @GetMapping("/{franchiseId}/characters")
    public ResponseEntity<HashSet<Character>> getCharactersInFranchise(
            @PathVariable int franchiseId
    ){
        HashSet<Character> returnCharacters = new HashSet<>();
        HttpStatus status = HttpStatus.OK;
        Franchise franchise;

        //if franchise exists, get its movies
        if(franchiseRepository.existsById(franchiseId)){
            franchise = franchiseRepository.findById(franchiseId).get();
            for (Movie movie : franchise.movies){
                for (Character character: movie.characters){
                    returnCharacters.add(character);
                }
            }
            } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacters, status);
    }
}
