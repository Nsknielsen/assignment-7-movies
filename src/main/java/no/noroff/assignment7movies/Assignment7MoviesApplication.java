package no.noroff.assignment7movies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;


@SpringBootApplication
public class Assignment7MoviesApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment7MoviesApplication.class, args);
    }

}
