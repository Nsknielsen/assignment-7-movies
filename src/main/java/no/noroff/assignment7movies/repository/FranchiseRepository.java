package no.noroff.assignment7movies.repository;

import no.noroff.assignment7movies.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
    Franchise findByName(String name);

    Boolean existsByName(String fullName);
}