package no.noroff.assignment7movies.repository;

import no.noroff.assignment7movies.models.Character;
import no.noroff.assignment7movies.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.HashSet;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
    Movie findMovieByTitle(String title);
}