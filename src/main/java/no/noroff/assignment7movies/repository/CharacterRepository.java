package no.noroff.assignment7movies.repository;

import no.noroff.assignment7movies.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;


//JPA repo interface for character table
public interface CharacterRepository extends JpaRepository<Character, Integer> {

}
