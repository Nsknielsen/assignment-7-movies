package no.noroff.assignment7movies.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Franchise{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "name")
    public String name;

    @Column(name = "description")
    public String description;


    //json getter to avoid infinite loop when getting a franchise
    @JsonGetter("movies")
    public Set<String> movies(){
        if(movies != null){
            return movies.stream()
                    .map(movie -> {
                        return "/movie/id/" + movie.id;
                    }).collect(Collectors.toSet());
        }
        return null;
    }

    //one-to-many mapping to movie in this franchise, to make a join table of franchise_id and movie_id
    @OneToMany(mappedBy = "franchise",fetch=FetchType.LAZY)
    public Set<Movie> movies = new HashSet<Movie>();


}