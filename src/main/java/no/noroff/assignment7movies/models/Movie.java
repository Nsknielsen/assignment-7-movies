package no.noroff.assignment7movies.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "title", nullable = false, length = 100)
    public String title;

    @Column(name = "genre")
    public String genre;

    @Column(name= "release_year", nullable = false, length = 10)
    public Integer releaseYear;

    @Column(name = "director", length = 40)
    public String director;

    @Column(name = "picture")
    public String picture;         //URL to movie poster

    @Column(name = "trailer")
    public String trailer;      //URL to YouTube trailer most likely

    //json getter to avoid infinite loop when getting a movie
    @JsonGetter("characters")
    public Set<String> characters(){
        if(characters != null){
            return characters.stream()
                    .map(character -> {
                        return "/character/id/" + character.id;
                    }).collect(Collectors.toSet());
        }
        return null;
    }


    //many-to-many mapping to characters in this movie, to make a join table of movie_id and character_id
    @ManyToMany(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE) //when this movie is deleted from db, also delete foreign keys in join tables referencing this movie.
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    public Set<Character> characters;




    //json getter to avoid infinite loop when getting a character
    @JsonGetter("franchise")
    public String franchise(){
        if(franchise != null){
            return "/franchise/id/" + franchise.id;
        }
        return null;
    }

    //many-to-one mapping to franchise in this movie, to make a join table of movie_id and franchise_id
    @ManyToOne()
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(
            name = "franchise_movie",
            joinColumns = { @JoinColumn(name = "movie_id") },
            inverseJoinColumns = { @JoinColumn(name = "franchise_id") }
    )
    public Franchise franchise;


}