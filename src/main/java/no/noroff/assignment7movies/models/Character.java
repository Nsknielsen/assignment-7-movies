package no.noroff.assignment7movies.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;


//character entity for use in table
@Entity
public class Character {

    //primary key
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "full_name", nullable = false, length = 40)
    public String fullName;

    @Column(length = 40)
    public String alias;

    @Column(length = 40)
    public String gender;

    @Column
    public String image;

    //json getter to avoid infinite loop when getting a character
    @JsonGetter("movies")
    public Set<String> movies(){
        if(movies != null){
            return movies.stream()
            .map(movie -> {
                return "/movie/id/" + movie.id;
            }).collect(Collectors.toSet());
        }
        return null;
    }

    //many-to-many mapping with movies table, to make a join table of movie_id and character_id
    @ManyToMany(mappedBy = "characters", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE) //when this movie is deleted from db, also delete foreign keys in join tables referencing this movie.
    public Set<Movie> movies;                   //made an assumption that this was safe to do since we do not consider the key pairs of
                                                //a join table "related data" (see assignment doc p.4)

}
