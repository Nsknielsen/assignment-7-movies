# Movie DB
##Description
RESTful api using Hibernate and PostgreSQL to handle requests for data about movies, characters, and franchises.
Has OpenApi documentation and a Swagger UI.

## Developers
@4ndersom Anders Madsen
@nsknielsen Nikolaj Nielsen

##How to use

The api is hosted live on Heroku.

Swagger UI for trying out endpoints: https://dbmovies7.herokuapp.com/swagger-ui/index.html

OpenAPI documentation of the api: https://dbmovies7.herokuapp.com/api-docs

To run the project locally, first start a Postgresql docker container with
`docker run -d --name postgres-demo -e POSTGRES_PASSWORD=supersecretpassword -e POSTGRES_DB=mydb -p 5432:5432 postgres:14-alpine`
Then run this project in IntelliJ. Default base URL for local instances is `http://localhost:8080`.

## Assumptions
Assignment docs page 4 says "You can do proper deletes, just ensure related
data is not deleted – the foreign keys can be set to null.". 
We interpreted this as saying that it is alright to use OnDeleteAction.CASCADE 
to clear out key pairs in join tables that refer to the entity being deleted,
since we do not consider the key pairs in a join table "data"
in its own right.

Assignment docs page 4 mentions "Updating characters in a movie".
We assumed that this should be a blanket replacement of movie.characters rather than a partial update
since there is no required functionality to delete characters from a movie,
and working with a blanket replacement at least allows clients to indirectly delete by passing
an empty array of character ID's to the update function.
